#include "checkerstcpsocket.h"
#include <QHostAddress>
#include <QtEndian>

const QString CheckersTcpSocket::SERVER_ADDRESS = "127.0.0.1";
const quint16 CheckersTcpSocket::SERVER_PORT = 1234;

CheckersTcpSocket::CheckersTcpSocket(QObject* parent)
    :QTcpSocket(parent)
{
    QObject::connect(this, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

bool CheckersTcpSocket::connectToCheckersServer()
{
    connectToHost(QHostAddress(SERVER_ADDRESS), SERVER_PORT);

    return waitForConnected(1000);
}

void CheckersTcpSocket::sendJoinGameRequest()
{
    sendRequest(Request::JOIN_GAME, "");
}

void CheckersTcpSocket::processResponse(Response response, ResponseStatus status, const QByteArray& payload)
{
    switch (response) {
    case Response::RESPONSE_JOIN_GAME:
        processJoinGameResponse(status, payload);
        break;
    case Response::NOTIFICATION_JOIN_GAME:
        processJoinGameNotification(status, payload);
        break;
    case Response::NOTIFICATION_MOVE:
        processMoveNotification(status, payload);
        break;
    case Response::NOTIFICATION_ENEMY_PLAYER_DISCONNECTED:
        emit enemyPlayerDisconnected();
        break;
    default:
        break;
    }
}

void CheckersTcpSocket::processJoinGameResponse(ResponseStatus status, const QByteArray& payload)
{
    if (status != ResponseStatus::SUCCESS)
    {
        return;
    }

    bool waitForSecondPlayer = static_cast<bool>(payload.at(0));
    Color playerColor = static_cast<Color>(getHostQuint32FromBytes(payload.mid(1, 4)));
    emit joinGameResponse(waitForSecondPlayer, playerColor);
}

void CheckersTcpSocket::processJoinGameNotification(ResponseStatus status, const QByteArray &payload)
{
    if (status != ResponseStatus::SUCCESS)
    {
        return;
    }

    Color playerColor = static_cast<Color>(getHostQuint32FromBytes(payload.left(4)));
    emit joinGameNotification(playerColor);
}

void CheckersTcpSocket::sendMoveRequest(FieldPos fromPos, FieldPos toPos)
{
    QByteArray payload;
    QDataStream stream(&payload, QIODevice::WriteOnly);

    quint32 fromRow = qToBigEndian(static_cast<quint32>(fromPos.row));
    stream.writeRawData((const char*)&fromRow, 4);
    quint32 fromCol = qToBigEndian(static_cast<quint32>(fromPos.col));
    stream.writeRawData((const char*)&fromCol, 4);

    quint32 toRow = qToBigEndian(static_cast<quint32>(toPos.row));
    stream.writeRawData((const char*)&toRow, 4);
    quint32 toCol = qToBigEndian(static_cast<quint32>(toPos.col));
    stream.writeRawData((const char*)&toCol, 4);

    sendRequest(Request::MOVE, payload);
}

void CheckersTcpSocket::processMoveNotification(ResponseStatus status, const QByteArray &payload)
{
    if (status != ResponseStatus::SUCCESS)
    {
        return;
    }

    emit moveNotification(gameStateUnserialize((const unsigned char*)payload.data()));
}

quint32 CheckersTcpSocket::getHostQuint32FromBytes(const QByteArray &bytes)
{
    return qFromBigEndian(*(quint32*)bytes.data());
}

void CheckersTcpSocket::onReadyRead()
{
    responseBuffer.append(readAll());

    while (responseBuffer.size() >= 12)
    {
        //read expected size of response
        quint32 expectedSize = getHostQuint32FromBytes(responseBuffer.mid(0, 4));

        //if whole message isn't yet composed, exit
        if (responseBuffer.size() < static_cast<int>(expectedSize))
        {
            return;
        }

        //read reponse code
        Response response = static_cast<Response>(getHostQuint32FromBytes(responseBuffer.mid(4, 4)));

        //read reponse status
        ResponseStatus status = static_cast<ResponseStatus>(getHostQuint32FromBytes(responseBuffer.mid(8, 4)));

        //read payload and process response
        QByteArray payload = responseBuffer.mid(12, expectedSize - 12);
        processResponse(response, status, payload);

        //remove processed response from buffer
        responseBuffer.remove(0, expectedSize);
    }
}

void CheckersTcpSocket::sendRequest(Request request, const QByteArray &payload)
{
    QByteArray msg;
    QDataStream stream(&msg, QIODevice::WriteOnly);
    //append size of request
    quint32 reqSize = qToBigEndian(static_cast<quint32>(8 + payload.size()));
    stream.writeRawData((const char*)&reqSize, 4);
    //append request code
    quint32 reqCode = qToBigEndian(static_cast<quint32>(request));
    stream.writeRawData((const char*)&reqCode, 4);
    //append payload
    stream.writeRawData(payload.constData(), payload.size());

    write(msg);
    flush();
}
