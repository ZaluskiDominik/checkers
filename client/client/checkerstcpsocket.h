#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <QTcpSocket>
#include <QByteArray>
#include <QDataStream>
extern "C" {
    #include "../../shared/game-api.h"
    #include "../../shared/response-status.h"
    #include "../../shared/responses.h"
    #include "../../shared/requests.h"
}

class CheckersTcpSocket : public QTcpSocket
{
    Q_OBJECT
public:
    static const QString SERVER_ADDRESS;
    static const quint16 SERVER_PORT;

    CheckersTcpSocket(QObject* parent);

    bool connectToCheckersServer();

    void sendJoinGameRequest();

    void processResponse(Response response, ResponseStatus status, const QByteArray& payload);

    void processJoinGameResponse(ResponseStatus status, const QByteArray& payload);

    void processJoinGameNotification(ResponseStatus status, const QByteArray& payload);

    void sendMoveRequest(FieldPos fromPos, FieldPos toPos);

    void processMoveNotification(ResponseStatus status, const QByteArray& payload);

    quint32 getHostQuint32FromBytes(const QByteArray& bytes);

private slots:
    void onReadyRead();

private:
    QByteArray responseBuffer;

    void sendRequest(Request request, const QByteArray& payload);

signals:
    void joinGameResponse(bool, Color);
    void joinGameNotification(Color);

    void moveNotification(GameState);

    void enemyPlayerDisconnected();
};

#endif // TCPSOCKET_H
