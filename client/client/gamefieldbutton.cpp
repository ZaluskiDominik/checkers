#include "gamefieldbutton.h"
#include <QPainter>

const QColor GameFieldButton::MOVE_HIGHLIGHT = Qt::darkBlue;
const QColor GameFieldButton::CAPTURE_HIGHLIGHT = Qt::red;

QImage GameFieldButton::lightFieldImg;
QImage GameFieldButton::darkFieldImg;

GameFieldButton::PieceImagesAggregator GameFieldButton::pieceImgAggregator;

GameFieldButton::GameFieldButton(FieldColor fieldColor, QWidget *parent)
    :QAbstractButton(parent), fieldColor(fieldColor)
{
    //load background field images, images of pieces if they weren't already loaded
    if (lightFieldImg.isNull())
    {
        lightFieldImg.load(":/img/img/field/light.png");
        darkFieldImg.load(":/img/img/field/dark.png");
        initPieceImgAggregator();
    }

    pieceColor = PieceColor::WHITE;
    fieldType = FieldType::EMPTY;
}

GameFieldButton::GameFieldButton(GameFieldButton::FieldColor fieldColor, GameFieldButton::PieceColor pieceColor, FieldType fieldType, QWidget *parent)
    :GameFieldButton(fieldColor, parent)
{
    this->pieceColor = pieceColor;
    this->fieldType = fieldType;
}

GameFieldButton::PieceColor GameFieldButton::mapColorToPieceColor(Color color)
{
    return (color == Color::WHITE) ? PieceColor::WHITE : PieceColor::BLACK;
}

void GameFieldButton::setFieldType(FieldType type)
{
    fieldType = type;
    update();
}

FieldType GameFieldButton::getFieldType()
{
    return fieldType;
}

void GameFieldButton::setPieceColor(GameFieldButton::PieceColor color)
{
    pieceColor = color;
    update();
}

GameFieldButton::PieceColor GameFieldButton::getPieceColor()
{
    return pieceColor;
}

void GameFieldButton::setHighlight(QColor color)
{
    highlightColor = color;
    update();
}

void GameFieldButton::resetHighlight()
{
    setHighlight(QColor());
}

void GameFieldButton::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    //draw field background
    QImage& bcgImg(fieldColor == FieldColor::LIGHT ? lightFieldImg : darkFieldImg);
    p.drawImage(0, 0, bcgImg.scaled(width(), height()));

    if (highlightColor.isValid())
    {
        p.setOpacity(0.6);
        p.fillRect(0, 0, width(), height(), highlightColor);
        p.setOpacity(1);
    }

    //draw piece if field is not empty
    QImage& pieceImg = getPieceImg();
    if (!pieceImg.isNull())
    {
        int length = std::min(width(), height()) * 1.25;
        p.drawImage((width() - length) / 2, (height() - length) / 2, pieceImg.scaled(length, length));
    }
}

void GameFieldButton::initPieceImgAggregator()
{
    pieceImgAggregator.black.king.load(":/img/img/black/king.png");
    pieceImgAggregator.black.pawn.load(":/img/img/black/pawn.png");
    pieceImgAggregator.white.king.load(":/img/img/white/king.png");
    pieceImgAggregator.white.pawn.load(":/img/img/white/pawn.png");
}

QImage &GameFieldButton::getPieceImg()
{
    switch (pieceColor) {
    case PieceColor::WHITE:
        switch (fieldType) {
        case FieldType::KING:
            return pieceImgAggregator.white.king;
        case FieldType::PAWN:
            return pieceImgAggregator.white.pawn;
        case FieldType::EMPTY:
            return pieceImgAggregator.white.empty;
        }
    case PieceColor::BLACK:
        switch (fieldType) {
        case FieldType::KING:
            return pieceImgAggregator.black.king;
        case FieldType::PAWN:
            return pieceImgAggregator.black.pawn;
        default:
            return pieceImgAggregator.black.empty;
        }
    }
}
