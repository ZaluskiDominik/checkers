#ifndef GAMEFIELD_H
#define GAMEFIELD_H

#include <QAbstractButton>
#include <QImage>
extern "C" {
    #include "../../shared/game-field.h"
}

class GameFieldButton : public QAbstractButton
{
public:
    enum class FieldColor
    {
        DARK = 0,
        LIGHT
    };

    enum class PieceColor
    {
        WHITE = 0,
        BLACK
    };

    struct PieceImages
    {
        QImage pawn;
        QImage king;
        QImage empty;
    };

    struct PieceImagesAggregator
    {
        PieceImages black;
        PieceImages white;
    };

    GameFieldButton(FieldColor fieldColor, QWidget *parent = nullptr);
    GameFieldButton(FieldColor fieldColor, PieceColor pieceColor, FieldType fieldType, QWidget *parent = nullptr);

    static const QColor MOVE_HIGHLIGHT;
    static const QColor CAPTURE_HIGHLIGHT;

    static PieceColor mapColorToPieceColor(Color color);

    void setFieldType(FieldType type);

    FieldType getFieldType();

    void setPieceColor(PieceColor color);

    PieceColor getPieceColor();

    void setHighlight(QColor color);
    void resetHighlight();

protected:
    void paintEvent(QPaintEvent *) override;

private:
    //shared images of dark and light background of field
    static QImage lightFieldImg;
    static QImage darkFieldImg;

    static PieceImagesAggregator pieceImgAggregator;

    const FieldColor fieldColor;

    //color of piece placed on this field
    PieceColor pieceColor;

    //what's currently placed on this field
    FieldType fieldType;

    QColor highlightColor;

    static void initPieceImgAggregator();

    QImage& getPieceImg();
};

#endif // GAMEFIELD_H
