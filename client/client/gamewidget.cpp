#include "gamewidget.h"
#include <QGridLayout>
#include <QPainter>
#include <QStyleOption>
#include "checkerstcpsocket.h"
#include <QMessageBox>

extern CheckersTcpSocket* socket;

GameWidget::GameWidget(QWidget *parent) : QWidget(parent)
{
    QHBoxLayout* mainLayout = new QHBoxLayout(this);
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
    setLayout(mainLayout);
    createWhoseTurnLabel(mainLayout);
    createBoardFields(mainLayout);

    QObject::connect(&blinkWhoseTurnTimer, SIGNAL(timeout()), this, SLOT(onBlinkWhoseTurnTimerTimeout()));
}

void GameWidget::initGame(GameFieldButton::PieceColor playerColor)
{
    this->playerColor = playerColor;

    //no piece is selected
    selectedPiece = FieldPos{-1, -1};
    gameStateInit(&gameState);

    syncWithGameState();
    blinkWhoseTurnTimer.start(1000);
    onBlinkWhoseTurnTimerTimeout();
}

void GameWidget::onFieldClicked()
{
    //if it's not this player turn, exit
    if (whoseTurn != playerColor)
    {
        return;
    }

    //find field that was clicked and its row, column
    int row, col;
    GameFieldButton* field = nullptr;
    for (int i = 0 ; i < BOARD_LENGTH && field == nullptr ; i++)
    {
        for (int j = 0 ; j < BOARD_LENGTH ; j++)
        {
            //if iterated field is the same field that was clicked exit loop
            if (sender() == fields[i][j])
            {
                row = i;
                col = j;
                field = fields[i][j];
                break;
            }
        }
    }

    //if piece with enemy color was clicked, exit
    if (gameState.fields[row][col].type != FieldType::EMPTY && field->getPieceColor() != playerColor)
    {
        return;
    }

    //if field with checkers piece was clicked mark it as selected or diselect it depending on whether
    //that piece is selected or not
    if (gameState.fields[row][col].type != FieldType::EMPTY)
    {
        fieldWithCheckersPieceClicked(field, row, col);
    }
    //else if empty field was clicked
    else
    {
        emptyFieldClicked(row, col);
    }
}

void GameWidget::onMoveNotification(GameState newGameState)
{
    gameState = newGameState;
    syncWithGameState();
}

void GameWidget::onBlinkWhoseTurnTimerTimeout()
{
    whoseTurnLabel->setStyleSheet(
        QString("border: 2px solid ") +
        ((!whoseTurnLabel->styleSheet().contains("green") && whoseTurn == playerColor)
            ? "green" : "rgb(232, 232, 232)")
    );
}

void GameWidget::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.fillRect(0, 0, 216, height(), QColor(232, 232, 232));
    p.fillRect(216, 0, 4, height(), Qt::gray);
    QWidget::paintEvent(event);
}

void GameWidget::highlightFieldsFromMovesList(const MovesList &moves)
{
    for (int i = 0 ; i < moves.size ; i++)
    {
        GameMove move = moves.moves[i];
        QColor highlightColor = (move.type == MoveType::CAPTURE) ?
            GameFieldButton::CAPTURE_HIGHLIGHT : GameFieldButton::MOVE_HIGHLIGHT;
        fields[move.pos.row][move.pos.col]->setHighlight(highlightColor);
    }
}

void GameWidget::resetFieldsHighlightFromMovesList(const MovesList &moves)
{
    for (int i = 0 ; i < moves.size ; i++)
    {
        FieldPos pos = moves.moves[i].pos;
        fields[pos.row][pos.col]->resetHighlight();
    }
}

void GameWidget::emptyFieldClicked(int row, int col)
{
    //if checkres piece isn't selected
    if (!fieldPosValidate(selectedPiece))
    {
        return;
    }

    MovesList availableMoves = gameAPIgetAvailableMoves(&gameState, selectedPiece);
    GameState gameStateBeforeMove = gameState;

    //if move is not valid return
    if (!gameAPIMove(&gameState, selectedPiece, FieldPos{row, col}))
    {
        return;
    }

    socket->sendMoveRequest(selectedPiece, FieldPos{row, col});

    bool moveFound;
    GameMove move = gameAPIGetMoveFromTo(&gameStateBeforeMove, selectedPiece, FieldPos{row, col}, &moveFound);
    //Remove highlight from old piece position
    fields[selectedPiece.row][selectedPiece.col]->resetHighlight();
    resetFieldsHighlightFromMovesList(availableMoves);

    //if after capture move there still are capture moves from new position of piece then
    //highlight, select checkers piece on new position and don't go to next turn
    if (move.type == MoveType::CAPTURE && gameAPIHasToCaptureFromPos(&gameState, FieldPos{row, col}))
    {
        selectedPiece = FieldPos{row, col};
        fields[row][col]->setHighlight(GameFieldButton::MOVE_HIGHLIGHT);
        highlightFieldsFromMovesList(gameAPIgetAvailableMoves(&gameState, FieldPos{row, col}));
    }
    //else go to next turn and remove selection
    else
    {
        //if piece reached row needed to transform to king, change field type to king
        int transformToKingRow = (playerColor == GameFieldButton::PieceColor::WHITE) ? BOARD_LENGTH - 1 : 0;
        if (row == transformToKingRow)
        {
            gameState.fields[row][col].type = FieldType::KING;
        }
        gameAPINextTurn(&gameState);
        selectedPiece = FieldPos{-1, -1};
    }

    syncWithGameState();
}

void GameWidget::fieldWithCheckersPieceClicked(GameFieldButton *field, int row, int col)
{
    //if some piece is selected, remove its highlightion
    if (fieldPosValidate(selectedPiece))
    {
        fields[selectedPiece.row][selectedPiece.col]->resetHighlight();
        resetFieldsHighlightFromMovesList(gameAPIgetAvailableMoves(&gameState, selectedPiece));
    }

    //if the same piece was clicked as selected, remove selection
    if (selectedPiece.row == row && selectedPiece.col == col)
    {
        selectedPiece = FieldPos{-1, -1};
    }
    //else set selection for clicked piece and highlight it
    else
    {
        selectedPiece = FieldPos{row, col};
        field->setHighlight(GameFieldButton::MOVE_HIGHLIGHT);
        highlightFieldsFromMovesList(gameAPIgetAvailableMoves(&gameState, selectedPiece));
    }
}

void GameWidget::syncWithGameState()
{
    whoseTurn = GameFieldButton::mapColorToPieceColor(gameState.whoseTurn);
    QString whoseTurnStrColor = (whoseTurn == GameFieldButton::PieceColor::WHITE) ? "white" : "black";
    whoseTurnLabel->setPixmap(QPixmap(":/img/img/" + whoseTurnStrColor + "/king.png"));

    for (int i = 0 ; i < BOARD_LENGTH ; i++)
    {
        for (int j = 0 ; j < BOARD_LENGTH ; j++)
        {
            fields[i][j]->setFieldType(gameState.fields[i][j].type);
            fields[i][j]->setPieceColor(GameFieldButton::mapColorToPieceColor(gameState.fields[i][j].color));
        }
    }

    checkIfGameEnded();
}

void GameWidget::createBoardFields(QHBoxLayout *parentLayout)
{
    QGridLayout* layout = new QGridLayout();
    layout->setSpacing(0);
    layout->setMargin(0);
    parentLayout->addLayout(layout);

    for (int i = 0 ; i < BOARD_LENGTH ; i++)
    {
        for (int j = 0 ; j < BOARD_LENGTH ; j++)
        {
            fields[i][j] = new GameFieldButton(
                ((j + i) % 2) ?
                GameFieldButton::FieldColor::DARK : GameFieldButton::FieldColor::LIGHT,
                this
            );
            fields[i][j]->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
            layout->addWidget(fields[i][j], i, j);
            QObject::connect(fields[i][j], SIGNAL(clicked()), this, SLOT(onFieldClicked()));
        }
    }
}

void GameWidget::createWhoseTurnLabel(QHBoxLayout *parentLayout)
{
    QVBoxLayout* layout = new QVBoxLayout();
    layout->setContentsMargins(10, 30, 10, 0);
    parentLayout->addLayout(layout);

    QLabel* textLabel = new QLabel(this);
    textLabel->setText("Whose turn");
    textLabel->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
    textLabel->setFixedHeight(40);
    textLabel->setStyleSheet("font-size: 32px; color: #555;");
    layout->addWidget(textLabel);

    whoseTurnLabel = new QLabel(this);
    whoseTurnLabel->setFixedSize(200, 200);
    whoseTurnLabel->setAlignment(Qt::AlignHCenter);
    whoseTurnLabel->setScaledContents(true);
    layout->addWidget(whoseTurnLabel);

    QWidget* spaceFiller = new QWidget(this);
    spaceFiller->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    layout->addWidget(spaceFiller);
}

void GameWidget::checkIfGameEnded()
{
    Color winner;
    if (gameAPIHasGameEnded(&gameState, &winner))
    {
        QString winnerStr = (winner == Color::WHITE ? "White" : "Black");
        QMessageBox::information(this, "Game has ended", "<b>" + winnerStr + "</b> player has won!");
        emit gameEnded();
    }
}
