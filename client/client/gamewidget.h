#ifndef GAMEWIDGET_H
#define GAMEWIDGET_H

#include <QWidget>
#include "gamefieldbutton.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QTimer>
extern "C" {
    #include "../../shared/game-api.h"
}

class GameWidget : public QWidget
{
    Q_OBJECT
public:
    explicit GameWidget(QWidget *parent = nullptr);

    static const int BOARD_LENGTH = 8;

    //initialize state of each new game
    void initGame(GameFieldButton::PieceColor playerColor);

private slots:
    void onFieldClicked();

    //enemy player moved
    void onMoveNotification(GameState newGameState);

    void onBlinkWhoseTurnTimerTimeout();

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    //checkers board fields as buttons
    GameFieldButton* fields[BOARD_LENGTH][BOARD_LENGTH];

    GameFieldButton::PieceColor whoseTurn;

    GameFieldButton::PieceColor playerColor;

    //object holding state of game used for checkers API functions
    GameState gameState;

    //position of currently selected(highlighted) player's piece
    //if no piece is selected selectedPiece = (-1, -1)
    FieldPos selectedPiece;

    QLabel* whoseTurnLabel;

    QTimer blinkWhoseTurnTimer;

    void highlightFieldsFromMovesList(const MovesList& moves);

    void resetFieldsHighlightFromMovesList(const MovesList& moves);

    void emptyFieldClicked(int row, int col);

    void fieldWithCheckersPieceClicked(GameFieldButton* field, int row, int col);

    //update client structures, images according to gameState object
    void syncWithGameState();

    void createBoardFields(QHBoxLayout* parentLayout);

    void createWhoseTurnLabel(QHBoxLayout* parentLayout);

    //if game hase ended displays message who won
    void checkIfGameEnded();

signals:
    void gameEnded();
};

#endif // GAMEWIDGET_H
