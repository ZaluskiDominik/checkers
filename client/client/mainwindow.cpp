#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QScreen>
#include <QMessageBox>
#include <QHBoxLayout>
#include <QTimer>

CheckersTcpSocket* socket;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    socket = new CheckersTcpSocket(this);
    centerPosition();
    setWindowTitle("Checkers");
    initWaitingForSecondPlayerLabel();

    QObject::connect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnectedFromServer()));
    QObject::connect(socket, SIGNAL(joinGameResponse(bool, Color)), this, SLOT(onJoinGameResponse(bool, Color)));
    QObject::connect(socket, SIGNAL(joinGameNotification(Color)), this, SLOT(onJoinGameNotification(Color)));
    QObject::connect(socket, SIGNAL(enemyPlayerDisconnected()), this, SLOT(onEnemyPlayerDisconnected()));
    QObject::connect(ui->gameWidget, SIGNAL(gameEnded()), this, SLOT(onGameEnded()));

    QObject::connect(socket, SIGNAL(moveNotification(GameState)), ui->gameWidget, SLOT(onMoveNotification(GameState)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::show()
{
    QMainWindow::show();

    if (!socket->connectToCheckersServer())
    {
        QMessageBox::warning(this, "Connection error", "Couldn't connect to checkers server! Program will be shut down...");
        QTimer::singleShot(10, this, SLOT(close()));
    }
}

void MainWindow::on_playButton_clicked()
{
    socket->sendJoinGameRequest();
}

void MainWindow::on_exitButton_clicked()
{
    close();
}

void MainWindow::onDisconnectedFromServer()
{
    QMessageBox::warning(this, "Disconnected", "Disconnected from checkers server! Program will be shut down...");
    close();
}

void MainWindow::onJoinGameResponse(bool waitForSecondPlayer, Color playerColor)
{
    if (waitForSecondPlayer)
    {
        ui->stackedWidget->setCurrentWidget(ui->gameRoomWidget);
    }
    else
    {
        changeViewToWhichColorWidget(playerColor);
    }
}

void MainWindow::onJoinGameNotification(Color playerColor)
{
    changeViewToWhichColorWidget(playerColor);
}

void MainWindow::onEnemyPlayerDisconnected()
{
    QMessageBox::information(this, "Enemy player disconnected", "Enemy player left the game. Back to menu...");
    onGameEnded();
}

void MainWindow::onGameEnded()
{
    ui->stackedWidget->setCurrentWidget(ui->menuWidget);
}

void MainWindow::centerPosition()
{
    QRect screenGeometry = QGuiApplication::primaryScreen()->geometry();
    int x = (screenGeometry.width() - width()) / 2;
    int y = (screenGeometry.height() - height()) / 2;
    move(x, y);
}

void MainWindow::initWaitingForSecondPlayerLabel()
{
    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget(ui->waitLabel);
    ui->frame->setLayout(layout);
}

void MainWindow::changeViewToWhichColorWidget(Color playerColor)
{
    ui->stackedWidget->setCurrentWidget(ui->whichColorWidget);
    QString color = (playerColor == Color::BLACK) ? "black" : "white";
    ui->playerColorLabel->setPixmap(QPixmap(":/img/img/" + color + "/king.png"));
    ui->gameWidget->initGame(GameFieldButton::mapColorToPieceColor(playerColor));
}

void MainWindow::on_startGameButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->gameWidget);
}
