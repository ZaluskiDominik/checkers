#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "checkerstcpsocket.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void show();

private slots:
    void on_playButton_clicked();

    void on_exitButton_clicked();

    void onDisconnectedFromServer();

    void onJoinGameResponse(bool waitForSecondPlayer, Color playerColor);

    void onJoinGameNotification(Color playerColor);

    void onEnemyPlayerDisconnected();

    void onGameEnded();

    void on_startGameButton_clicked();

private:
    Ui::MainWindow *ui;

    void centerPosition();

    void initWaitingForSecondPlayerLabel();

    void changeViewToWhichColorWidget(Color playerColor);
};
#endif // MAINWINDOW_H
