#ifndef CLIENT_DATA_H
#define CLIENT_DATA_H

#include "game-data.h"
#include "../shared/color.h"

struct ClientData
{
    int socket;
    Color color;
    struct GameData* game;
};

#endif