#include "clients-list.h"
#include <stddef.h>
#include <unistd.h>

void clientsListInit(struct ClientsList* clients)
{
    clients->size = 0;
    pthread_mutex_init(&clients->mutex, NULL);
}

bool _clientsListAddHelper(struct ClientsList* clients, struct ClientData clientData, bool threadSave)
{
    if (threadSave)
        pthread_mutex_lock(&clients->mutex);
    if (clients->size >= CLIENTS_LIST_MAX_SIZE)
    {
        if (threadSave)
            pthread_mutex_unlock(&clients->mutex);
        
        return false;
    }

    clients->clients[clients->size++] = clientData;
    if (threadSave)
        pthread_mutex_unlock(&clients->mutex);

    return true;
}

bool clientsListThreadSaveAdd(struct ClientsList* clients, struct ClientData clientData)
{
    return _clientsListAddHelper(clients, clientData, true);
}

bool clientsListAdd(struct ClientsList* clients, struct ClientData clientData)
{
    return _clientsListAddHelper(clients, clientData, false);
}

struct ClientData* _clientsListFindBySocketHelper(struct ClientsList* clients, int socket, bool threadSave)
{
    if (threadSave)
        pthread_mutex_lock(&clients->mutex);
    for (int i = 0 ; i < clients->size ; i++)
    {
        if (clients->clients[i].socket == socket)
        {
            if (threadSave)
                pthread_mutex_unlock(&clients->mutex);
            
            return &clients->clients[i];
        }
    }
    if (threadSave)
        pthread_mutex_unlock(&clients->mutex);

    return NULL;
}

struct ClientData* clientsListThreadSaveFindBySocket(struct ClientsList* clients, int socket)
{
    return _clientsListFindBySocketHelper(clients, socket, true);
}

struct ClientData* clientsListFindBySocket(struct ClientsList* clients, int socket)
{
    return _clientsListFindBySocketHelper(clients, socket, false);
}

void clientsListRemoveBySocket(struct ClientsList* clients, int socket)
{
    int index = 0;
    for ( ; index < clients->size && clients->clients[index].socket != socket ; index++);
    
    for (int i = index ; i < clients->size - 1 ; i++)
    {
        clients->clients[i] = clients->clients[i + 1];
        //if client is in game, game's players addresses needs update
        if (clients->clients[i].game != NULL)
        {
            pthread_mutex_lock(&clients->clients[i].game->mutex);
            //after shifting client to left update responding payers pointers to new client address
            for (int j = 0 ; j < clients->clients[i].game->numPlayers ; j++)
            {
                clients->clients[i].game->players[j] = &clients->clients[i];
            }
            pthread_mutex_unlock(&clients->clients[i].game->mutex);
        }
    }
    clients->size -= (index < clients->size);   
}

void clientsListThreadSaveRemoveBySocket(struct ClientsList* clients, int socket)
{
    pthread_mutex_lock(&clients->mutex);
    clientsListRemoveBySocket(clients, socket);
    pthread_mutex_unlock(&clients->mutex);
}

void clientsListFree(struct ClientsList* clients)
{
    pthread_mutex_destroy(&clients->mutex);
}