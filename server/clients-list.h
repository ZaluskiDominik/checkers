#ifndef CLIENTS_LIST_H
#define CLIENTS_LIST_H

#include "client-data.h"
#include <stdbool.h>
#include <pthread.h>

#define CLIENTS_LIST_MAX_SIZE 100

struct ClientsList
{
    struct ClientData clients[CLIENTS_LIST_MAX_SIZE];
    int size;
    pthread_mutex_t mutex;
};

void clientsListInit(struct ClientsList* clients);

//same as clientsListAdd but with locking mutex on clients list
bool clientsListThreadSaveAdd(struct ClientsList* clients, struct ClientData clientData);

//adds new client to clients list, returns true if client could be added(didn't reach list max size)
bool clientsListAdd(struct ClientsList* clients, struct ClientData clientData);

//same as clientsListFindBySocket but with locking mutex on clients list
struct ClientData* clientsListThreadSaveFindBySocket(struct ClientsList* clients, int socket);

//returns client searching by given socket or NULL if client not found in list
struct ClientData* clientsListFindBySocket(struct ClientsList* clients, int socket);

//same as clientsListRemoveBySocket but with locking mutex on clients list
void clientsListThreadSaveRemoveBySocket(struct ClientsList* clients, int socket);

//removes client from clients list searching by socket
void clientsListRemoveBySocket(struct ClientsList* clients, int socket);

void clientsListFree(struct ClientsList* clients);

#endif