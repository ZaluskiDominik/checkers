#include "debug.h"
#include <stdio.h>
#include "server-config.h"
#include <pthread.h>
#include <string.h>
#include "games-list.h"

extern struct GamesList* games;

pthread_mutex_t printMutex;

void appendSocketToTag(int socket, const char* tag, char* resBuffer)
{
    char strSocket[20];
    sprintf(strSocket, " [socket=%d]", socket);
    strcpy(resBuffer, tag);
    strcpy(resBuffer + strlen(tag), strSocket);
}

void debugInit()
{
    #ifdef SERVER_DEBUG
        pthread_mutex_init(&printMutex, NULL);
    #endif
}

void debugFree()
{
    #ifdef SERVER_DEBUG
        pthread_mutex_destroy(&printMutex);
    #endif
}

void debugHexBuffer(const char* tag, const unsigned char* buffer, int size)
{
    #ifdef SERVER_DEBUG
        pthread_mutex_lock(&printMutex);
        printf("%s:", tag);
        for (int i = 0; i < size; i ++)
        {
            printf(" %02x", buffer[i]);
        }
        printf("\n");
        pthread_mutex_unlock(&printMutex);
    #endif
}

void socketDebugHexBuffer(int socket, const char* tag, const unsigned char* buffer, int size)
{
    char newTag[strlen(tag) + 20];
    appendSocketToTag(socket, tag, newTag);
    debugHexBuffer(newTag, buffer, size);
}

void debugInt(const char* tag, int value)
{
    #ifdef SERVER_DEBUG
        pthread_mutex_lock(&printMutex);
        printf("%s: %d\n", tag, value);
        pthread_mutex_unlock(&printMutex);
    #endif
}

void socketDebugInt(int socket, const char* tag, int value)
{
    char newTag[strlen(tag) + 20];
    appendSocketToTag(socket, tag, newTag);
    debugInt(newTag, value);
}

void debug(const char* tag)
{
    #ifdef SERVER_DEBUG
        pthread_mutex_lock(&printMutex);
        printf("%s\n", tag);
        pthread_mutex_unlock(&printMutex);
    #endif
}

void socketDebug(int socket, const char* tag)
{
    char newTag[strlen(tag) + 20];
    appendSocketToTag(socket, tag, newTag);
    debug(newTag);
}

void debugGamesListContent()
{
    #ifdef SERVER_DEBUG
    pthread_mutex_lock(&printMutex);
    printf("-------------------------------------\nGamesList size: %d\n", games->size);
    for (int i = 0 ; i < games->size ; i++)
    {
        printf("Game addr=%p, Players: ", &games->games[i]);
        for (int j = 0 ; j < games->games[i].numPlayers ; j++)
        {
            printf("socket=%d, client->game=%p | ", games->games[i].players[j]->socket, games->games[i].players[j]->game);
        }
        printf("\n-------------------------------------\n");
    }
    pthread_mutex_unlock(&printMutex);
    #endif
}