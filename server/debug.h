#ifndef DEBUG_H
#define DEBUG_H

/*
Thread save debug info printer
Prints tag - value pairs for various data types
//Tag is a description of debugged value
*/

void debugInit();

void debugFree();

//Prints hex content of buffer
void debugHexBuffer(const char* tag, const unsigned char* buffer, int size);

//Same as debugHexBuffer but also prints socket handle
void socketDebugHexBuffer(int socket, const char* tag, const unsigned char* buffer, int size);

//Prints integer
void debugInt(const char* tag, int value);

//Same as debugInt but also prints socket handle
void socketDebugInt(int socket, const char* tag, int value);

//Prints debug message
void debug(const char* tag);

//Same as debug but also prints socket handle
void socketDebug(int socket, const char* tag);

//prints content of GamesList list
void debugGamesListContent();

#endif