#ifndef GAME_DATA_H
#define GAME_DATA_H

#include "client-data.h"
#include <pthread.h>
#include "../shared/game-state.h"

struct GameData
{
    pthread_mutex_t mutex;
    struct ClientData* players[2];
    int numPlayers;
    struct GameState* gameState;
};

#endif