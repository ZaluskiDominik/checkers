#include "games-list.h"
#include "debug.h"
#include <stdlib.h>

void gamesListInit(struct GamesList* games)
{
    games->size = 0;
    pthread_mutex_init(&games->mutex, NULL);
}

bool _gamesListAddHelper(struct GamesList* games, struct ClientData* client, bool threadSave)
{
    if (threadSave)
        pthread_mutex_lock(&games->mutex);

    if (games->size >= GAMES_LIST_MAX_SIZE)
    {
        if (threadSave)
            pthread_mutex_unlock(&games->mutex);
        return false;
    }

    struct GameData game;
    game.players[0] = client;
    game.numPlayers = 1;
    game.gameState = NULL;
    pthread_mutex_init(&game.mutex, NULL);

    games->games[games->size++] = game;
    client->game = &games->games[games->size - 1];

    if (threadSave)
        pthread_mutex_unlock(&games->mutex);

    return true;
}

bool gamesListThreadSaveAdd(struct GamesList* games, struct ClientData* client)
{
    return _gamesListAddHelper(games, client, true);
}

bool gamesListAdd(struct GamesList* games, struct ClientData* client)
{
    return _gamesListAddHelper(games, client, false);
}

void _gamesListRemoveHelper(struct GamesList* games, struct GameData* game, bool threadSave)
{
    if (threadSave)
        pthread_mutex_lock(&games->mutex);

    int index = 0;
    for ( ; index < games->size && &games->games[index] != game ; index++);
    if (index < games->size)
    {
        pthread_mutex_lock(&games->games[index].mutex);
        for (int i = 0 ; i < games->games[index].numPlayers ; i++)
        {
            games->games[index].players[i]->game = NULL;
        }
        if (game->gameState != NULL)
        {
            free(game->gameState);
        }
        pthread_mutex_unlock(&games->games[index].mutex);
        
        pthread_mutex_destroy(&games->games[index].mutex);
        
        for (int i = index ; i < games->size - 1 ; i++)
        {
            pthread_mutex_lock(&games->games[i].mutex);
            games->games[i] = games->games[i + 1];
            //update game address in clients data after shifthing games left
            for (int j = 0 ; j < games->games[i].numPlayers ; j++)
            {
                games->games[i].players[j]->game = &games->games[i];
            }
            pthread_mutex_unlock(&games->games[i].mutex);
        }
        games->size--;

        debugInt("Removed game, games number", games->size);
    }

    if (threadSave)
        pthread_mutex_unlock(&games->mutex);
}

void gamesListThreadSaveRemove(struct GamesList* games, struct GameData* game)
{
    _gamesListRemoveHelper(games, game, true);
}

void gamesListRemove(struct GamesList* games, struct GameData* game)
{
    _gamesListRemoveHelper(games, game, false);
}

void gamesListFree(struct GamesList* games)
{
    pthread_mutex_lock(&games->mutex);
    for (int i = 0 ; i < games->size ; i++)
    {
        if (games->games[i].gameState != NULL)
        {
            free(games->games[i].gameState);
        }
        pthread_mutex_destroy(&games->games[i].mutex);
    }
    pthread_mutex_unlock(&games->mutex);

    pthread_mutex_destroy(&games->mutex);
}