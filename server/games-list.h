#ifndef GAMES_LIST_H
#define GAMES_LIST_H

#include "game-data.h"
#include <stdbool.h>
#include <pthread.h>

#define GAMES_LIST_MAX_SIZE 1000

struct GamesList
{
    struct GameData games[GAMES_LIST_MAX_SIZE];
    int size;
    pthread_mutex_t mutex;
};

/*
Usage same sa for ClientsList
See client-list.h for reference
*/

void gamesListInit(struct GamesList* games);

bool gamesListThreadSaveAdd(struct GamesList* games, struct ClientData* client);

bool gamesListAdd(struct GamesList* games, struct ClientData* client);

void gamesListThreadSaveRemove(struct GamesList* games, struct GameData* game);

void gamesListThreadSaveRemove(struct GamesList* games, struct GameData* game);

void gamesListRemove(struct GamesList* games, struct GameData* game);

void gamesListFree(struct GamesList* games);

#endif