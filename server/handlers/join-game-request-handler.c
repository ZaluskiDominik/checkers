#include "request-handler.h"
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <arpa/inet.h>
#include "../debug.h"
#include "../../shared/game-state.h"
#include "../sender.h"

void createNewGameState(struct ClientData* client)
{
    struct GameState* gameState = malloc(sizeof(struct GameState));
    gameStateInit(gameState);
    client->game->gameState = gameState;
}

//sends to issuer of JOIN_GAME request response that join was succesful and whether there already
//was player in joined room
//waitForOtherPlayer - when true client must wait for second player to join, else game can be started
//playerCode - important only when waitForOtherPlayer = false
void sendSuccessResponse(struct ClientData* client, bool waitForOtherPlayer, Color playerColor)
{
    unsigned char payload[sizeof(uint32_t) + 2];
    payload[0] = (char)waitForOtherPlayer;
    uint32_t colorCode = htonl((uint32_t)playerColor);
    memcpy(payload + 1, &colorCode, sizeof(uint32_t));
    payload[sizeof(uint32_t) + 1] = '\0';
    sendResponse(client->socket, RESPONSE_JOIN_GAME, SUCCESS, payload, sizeof(uint32_t) + 1);
}

//sends to player that was first in waiting room, notify that client that second player joined
//and game can be started
void sendJoinGameNotification(struct ClientData* client, Color playerColor)
{
    unsigned char payload[sizeof(uint32_t) + 1];
    uint32_t colorCode = htonl((uint32_t)playerColor);
    memcpy(payload, &colorCode, sizeof(uint32_t));
    payload[sizeof(uint32_t)] = '\0';
    sendResponse(client->socket, NOTIFICATION_JOIN_GAME, SUCCESS, payload, sizeof(uint32_t));
}

void joinGameRequestHandler(struct ClientData* client, const unsigned char* payload)
{
    pthread_mutex_lock(&games->mutex);
    bool foundGame = false;

    for (int i = 0 ; i < games->size ; i++)
    {
        pthread_mutex_lock(&games->games[i].mutex);
        //if game room has one player waiting for another player to join
        //then join and start a game
        if (games->games[i].numPlayers == 1)
        {
            games->games[i].players[1] = client;
            games->games[i].numPlayers++;
            client->game = &games->games[i];
            createNewGameState(client);
            pthread_mutex_unlock(&games->games[i].mutex);

            socketDebugInt(client->socket, "Joined game room, players number", 2);
            foundGame = true;
            //player that joined first has white pieces
            client->game->players[0]->color = WHITE;
            sendJoinGameNotification(client->game->players[0], WHITE);
            //player that joined second has black pieces
            client->game->players[1]->color = BLACK;
            sendSuccessResponse(client, false, BLACK);
            break;
        }
        pthread_mutex_unlock(&games->games[i].mutex);
    }

    //if all game rooms has games in progress then create a new one
    if (!foundGame)
    {
        gamesListAdd(games, client);
        sendSuccessResponse(client, true, WHITE);
        socketDebugInt(client->socket, "New game room, players number", 1);
    }
    pthread_mutex_unlock(&games->mutex);
}