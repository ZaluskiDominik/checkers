#include "request-handler.h"
#include <stdint.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <string.h>
#include "../../shared/game-api.h"
#include "../debug.h"

bool validateRequestAndGameState(struct GameData* game, struct ClientData* client, const unsigned char* payload, int payloadSize)
{
    if (payloadSize < 4 * sizeof(uint32_t))
    {
        sendAsciiResponse(client->socket, RESPONSE_MOVE, FAILURE, "Missing required parameters");
        socketDebug(client->socket, "RESPONSE_MOVE, FAILURE, Missing required parameters");
        return false;
    }

    if (game == NULL || game->gameState == NULL)
    {
        sendAsciiResponse(client->socket, RESPONSE_MOVE, FAILURE, "Game not started");
        socketDebug(client->socket, "RESPONSE_MOVE, FAILURE, Game not started");
        socketDebugInt(client->socket, "RESPONSE_MOVE, FAILURE, game == NULL", (int)(game==NULL));
        return false;
    }
    
    if (client->color != game->gameState->whoseTurn)
    {
        sendAsciiResponse(client->socket, RESPONSE_MOVE, FAILURE, "Not your turn");
        socketDebug(client->socket, "RESPONSE_MOVE, FAILURE, Not your turn");
        return false;
    }

    return true;
}

void sendMoveNotification(struct ClientData* client)
{
    struct ClientData* enemyPlayer = (client->game->players[0] == client) ? 
        client->game->players[1] : client->game->players[0];
    
    unsigned char serializedGameState[GAME_STATE_SERIALIZED_SIZE + 1];
    gameStateSerialize(client->game->gameState, serializedGameState);
    serializedGameState[GAME_STATE_SERIALIZED_SIZE] = '\0';
    
    socketDebugInt(client->socket, "send NOTIFICATION_MOVE to socket", enemyPlayer->socket);
    sendResponse(enemyPlayer->socket, NOTIFICATION_MOVE, SUCCESS, serializedGameState, GAME_STATE_SERIALIZED_SIZE);
}

void moveRequestHandler(struct ClientData* client, const unsigned char* payload, int payloadSize)
{
    struct GameData* game = client->game;

    if (!validateRequestAndGameState(game, client, payload, payloadSize))
    {
        return;
    }

    //retrieve start position of move from payload
    uint32_t row = ntohl(*(uint32_t*)payload);
    uint32_t col = ntohl(*(uint32_t*)(payload + sizeof(uint32_t)));
    struct FieldPos fromPos = {row, col};
    
    //retrieve end position of move from payload
    row = ntohl(*(uint32_t*)(payload + (2*sizeof(uint32_t))));
    col = ntohl(*(uint32_t*)(payload + (3*sizeof(uint32_t))));
    struct FieldPos toPos = {row, col};

    pthread_mutex_lock(&game->mutex);

    bool moveFound;
    struct GameMove move = gameAPIGetMoveFromTo(game->gameState, fromPos, toPos, &moveFound);

    //validate move of client and if it's valid execute it, else return FAILURE response
    if (!gameAPIMove(game->gameState, fromPos, toPos))
    {
        pthread_mutex_unlock(&game->mutex);
        sendAsciiResponse(client->socket, RESPONSE_MOVE, FAILURE, "Not valid move");
        socketDebug(client->socket, "RESPONSE_MOVE, FAILURE, Not valid move");
        return;
    }

    //go to next turn if it was regular move
    //or capture move but client has no more pieces to capture from new position(capture is mandatory)
    if (move.type == REGULAR || !gameAPIHasToCaptureFromPos(game->gameState, toPos))
    {
        //if piece reached row needed to transform to king, change field type to king
        int transformToKingRow = (client->color == WHITE) ? GAME_FIELD_LENGTH - 1 : 0;
        if (toPos.row == transformToKingRow)
        {
            game->gameState->fields[toPos.row][toPos.col].type = KING;
        }
        gameAPINextTurn(game->gameState);
        socketDebugInt(client->socket, "MOVE request, change turn to color: ", game->gameState->whoseTurn);
    }

    sendEmptyResponse(client->socket, RESPONSE_MOVE, SUCCESS);
    sendMoveNotification(client);

    pthread_mutex_unlock(&game->mutex);

    Color winner;
    if (gameAPIHasGameEnded(game->gameState, &winner))
    {
        socketDebugInt(client->socket, "Game ended, winner color", winner);
        gamesListThreadSaveRemove(games, client->game);
    }

    socketDebug(client->socket, "RESPONSE_MOVE, SUCCESS");   
}