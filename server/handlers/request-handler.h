#ifndef REQUEST_HANDLER_H
#define REQUEST_HANDLER_H

#include "../clients-list.h"
#include "../games-list.h"
#include "../sender.h"

extern int serverSocket;

extern struct ClientsList* clients;

extern struct GamesList* games;

#endif