#include "server.h"
#include <signal.h>
#include <stdlib.h>

void interruptSigHandler(int sig)
{
    serverFree();
    exit(0);
}

int main()
{
    signal(SIGINT, interruptSigHandler);
    serverStart();

    return 0;
}