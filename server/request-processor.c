#include "request-processor.h"
#include <stdio.h>
#include "clients-list.h"
#include "debug.h"
#include <string.h>

extern struct ClientsList* clients;

extern pthread_mutex_t printMutex;

void joinGameRequestHandler(struct ClientData* client, const unsigned char* payload);
void moveRequestHandler(struct ClientData* client, const unsigned char* payload, int payloadSize);

void processRequest(int clientSocket, const unsigned char* payload, int payloadSize, Request request)
{
    struct ClientData* client = clientsListFindBySocket(clients, clientSocket);
    if (client == NULL)
    {
        socketDebugHexBuffer(clientSocket, "Process request, client not found, payload", payload, payloadSize);
        return;
    }

    switch (request)
    {
        case JOIN_GAME:
            socketDebugHexBuffer(client->socket, "JOIN_GAME request, payload", payload, payloadSize);
            joinGameRequestHandler(client, payload);
            break;
        case MOVE:
            socketDebugHexBuffer(client->socket, "MOVE request, payload", payload, payloadSize);
            moveRequestHandler(client, payload, payloadSize);
            break;
        case INVALID:
        case REQUESTS_COUNT:
            socketDebugHexBuffer(clientSocket, "INVALID request, payload", payload, payloadSize);
            break;
    }
    debugGamesListContent();
}