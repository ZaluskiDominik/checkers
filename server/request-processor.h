#ifndef REQUESTS_PROCESSOR_H
#define REQUESTS_PROCESSOR_H

#include "../shared/requests.h"
#include "clients-list.h"
#include "games-list.h"

void processRequest(int clientSocket, const unsigned char* payload, int payloadSize, Request request);

#endif