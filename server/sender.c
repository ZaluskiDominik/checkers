#include "sender.h"
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdint.h>
#include "debug.h"

void sendResponse(int socket, Response response, ResponseStatus status, const unsigned char* payload, int payloadSize)
{
    int responseSize = 3 * sizeof(uint32_t) + payloadSize;
    unsigned char buffer[responseSize];

    uint32_t expectedSize = htonl((uint32_t)responseSize);
    memcpy(buffer, &expectedSize, sizeof(uint32_t));
    uint32_t responseCode = htonl((uint32_t)response);
    memcpy(buffer + sizeof(uint32_t), &responseCode, sizeof(uint32_t));
    uint32_t statusCode = htonl((uint32_t)status);
    memcpy(buffer + (2*sizeof(uint32_t)), &statusCode, sizeof(uint32_t));

    memcpy(buffer + (3*sizeof(uint32_t)), payload, payloadSize);
    write(socket, buffer, responseSize);
    socketDebugHexBuffer(socket, "Response content", buffer, responseSize);
}

void sendAsciiResponse(int socket, Response response, ResponseStatus status, const char* msg)
{
    sendResponse(socket, response, status, (const unsigned char*)msg, strlen(msg));
}

void sendEmptyResponse(int socket, Response response, ResponseStatus status)
{
    sendAsciiResponse(socket, response, status, "");
}