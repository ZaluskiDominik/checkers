#ifndef SENDER_H
#define SENDER_H

#include "../shared/responses.h"
#include "../shared/response-status.h"

//Sends response to client with given socket, payload MUST be NULL terminated
void sendResponse(int socket, Response response, ResponseStatus status, const unsigned char* payload, int payloadSize);

void sendAsciiResponse(int socket, Response response, ResponseStatus status, const char* msg);

void sendEmptyResponse(int socket, Response response, ResponseStatus status);

#endif