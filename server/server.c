#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include "server-config.h"
#include "server.h"
#include <stdint.h>
#include "request-processor.h"
#include "debug.h"
#include "sender.h"

struct thread_data_t
{
    int clientSocket;
};

int serverSocket;

struct ClientsList* clients;

struct GamesList* games;

int createServerSocket()
{
    int sSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (sSocket < 0)
    {
        fprintf(stderr, "An error occured while creating a server socket: ");
        perror(NULL);
        exit(1);
    }

    int flag = 1;
    setsockopt(sSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&flag, sizeof(flag)); 

    return sSocket;
}

void bindServerSocket()
{
    struct sockaddr_in saddr;
    memset(&saddr, 0, sizeof(struct sockaddr));
    saddr.sin_family = AF_INET;
    inet_pton(AF_INET, SERVER_ADDRESS, &saddr.sin_addr.s_addr);
    saddr.sin_port = htons(SERVER_PORT);

    if (bind(serverSocket, (struct sockaddr*)&saddr, sizeof(struct sockaddr)) < 0)
    {
        fprintf(stderr, "An error occured while binding a server socket: ");
        perror(NULL);
        close(serverSocket);
        exit(1);
    }
}

void listenForConnections()
{
    if (listen(serverSocket, SERVER_QUEUE_SIZE) < 0)
    {
        fprintf(stderr, "An error occured while trying to listen on server socket: ");
        perror(NULL);
        close(serverSocket);
        exit(1);
    }
}

//Returns expected size of whole request from client - first 4 bytes of request
int getRequestExpectedSize(const unsigned char* requestBuffer)
{    
    return ntohl(*(uint32_t*)requestBuffer);
}

//Returns request type(one of Request enum from requests.h) - second 4 bytes of request
Request getRequestType(const unsigned char* requestBuffer)
{
    int code = ntohl(*(uint32_t*)(requestBuffer + sizeof(uint32_t)));
    if (code >= REQUESTS_COUNT)
    {
        return INVALID;
    }

    return (Request)code;
}

//Loop composing chunks of client request into whole request, runs until client disconnects
void clientRequestsLoop(int clientSocket)
{
    int expectedSize;
    int chunkSize = 0;
    int readSize = 0;
    unsigned char requestBuffer[SERVER_MAX_REQUEST_BUFFER_SIZE];
    while ((chunkSize = read(clientSocket, requestBuffer + readSize, SERVER_MAX_REQUEST_BUFFER_SIZE - readSize)))
    {
        readSize += chunkSize;
        socketDebugInt(clientSocket, "Request buffer size", readSize);
        while (readSize >= sizeof(uint32_t)*2 && readSize >= (expectedSize = getRequestExpectedSize(requestBuffer)))
        {
            socketDebugInt(clientSocket, "Proceed request size", expectedSize);
            socketDebugHexBuffer(clientSocket, "Request buffer", requestBuffer, readSize);
            //retrieve payload terminated with null from request
            int payloadSize = expectedSize - (2*sizeof(uint32_t));
            unsigned char payload[payloadSize + 1];
            memcpy(payload, requestBuffer + (2*sizeof(uint32_t)), payloadSize);
            payload[payloadSize] = '\0';

            processRequest(clientSocket, payload, payloadSize, getRequestType(requestBuffer));

            //remove processed part of request buffer, shift rest of request buffer to the beginning of buffer 
            memcpy(requestBuffer, requestBuffer + expectedSize, readSize - expectedSize);
            readSize -= expectedSize;
        }
    }
}

void clientDisconnected(int clientSocket)
{
    pthread_mutex_lock(&clients->mutex);
    struct ClientData* client = clientsListFindBySocket(clients, clientSocket);
    //if game is in progress send notification to enemy about disconnection from server
    if (client->game != NULL)
    {
        pthread_mutex_lock(&client->game->mutex);
        if (client->game->gameState != NULL && client->game->numPlayers > 1)
        {

            struct ClientData* enemy = (client == client->game->players[0]) ? 
                client->game->players[1] : client->game->players[0];

            socketDebug(enemy->socket, "Sending NOTIFICATION_ENEMY_PLAYER_DISCONNECTED");
            sendEmptyResponse(enemy->socket, NOTIFICATION_ENEMY_PLAYER_DISCONNECTED, SUCCESS);
        }
        pthread_mutex_unlock(&client->game->mutex);
        gamesListThreadSaveRemove(games, client->game);
    }
    clientsListRemoveBySocket(clients, clientSocket);
    pthread_mutex_unlock(&clients->mutex);

    socketDebugInt(clientSocket, "Client disconnected, clients number", clients->size);
}

//Ran for every client after connection in separate thread
void *clientThread(void *tData)
{
    pthread_detach(pthread_self());
    struct thread_data_t *data = (struct thread_data_t*)tData;
    int clientSocket = data->clientSocket;
    free(data);

    struct ClientData client;
    client.socket = clientSocket;
    client.game = NULL;
    if (clientsListThreadSaveAdd(clients, client))
    {
        socketDebugInt(clientSocket, "Client added, clients number", clients->size);
        clientRequestsLoop(clientSocket);
        clientDisconnected(clientSocket);
    }

    pthread_exit(NULL);
}

//New connection handler
void handleClientConnection(int clientSocket)
{
    pthread_t thread;
    struct thread_data_t* data = malloc(sizeof(struct thread_data_t));
    data->clientSocket = clientSocket;

    if (pthread_create(&thread, NULL, clientThread, (void*)data))
    {
        fprintf(stderr, "Couldn't create thread for client\n");
        close(clientSocket);
    }
}

//Server loop receiving new client connections
void serverLoop()
{
    while (1)
    {
       int clientSocket = accept(serverSocket, NULL, NULL);
       if (clientSocket < 0)
       {
           fprintf(stderr, "Couldn't create socket for client connection\n");
           continue;
       }

       handleClientConnection(clientSocket);
    }
}

void serverStart()
{
    clients = malloc(sizeof(struct ClientsList));
    clientsListInit(clients);

    games = malloc(sizeof(struct GamesList));
    gamesListInit(games);

    debugInit();

    serverSocket = createServerSocket();
    bindServerSocket();
    listenForConnections();
    serverLoop();
}

void serverFree()
{
    clientsListFree(clients);
    free(clients);
    gamesListFree(games);
    free(games);
    debugFree();
    close(serverSocket);
    printf("Closing server...\n");
}