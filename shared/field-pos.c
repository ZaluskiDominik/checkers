#include "field-pos.h"

bool fieldPosValidate(struct FieldPos pos)
{
    return pos.row >= 0 && pos.row < GAME_FIELD_LENGTH && pos.col >= 0 && pos.col < GAME_FIELD_LENGTH;
}