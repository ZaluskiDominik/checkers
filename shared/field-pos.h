#ifndef FIELD_POS_H
#define FIELD_POS_H

#include <stdbool.h>
#include "color.h"

#define GAME_FIELD_LENGTH 8

struct FieldPos
{
    int row;
    int col;
};

bool fieldPosValidate(struct FieldPos pos);

#endif