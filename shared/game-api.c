#include "game-api.h"
#include <math.h>
#include <stdlib.h>

bool gameAPIMove(struct GameState* gameState, struct FieldPos fromPos, struct FieldPos toPos)
{
    struct MovesList availableMoves = gameAPIgetAvailableMoves(gameState, fromPos);
    for (int i = 0 ; i < availableMoves.size ; i++)
    {
        struct GameMove* move = &availableMoves.moves[i];
        //if from and to positions are the same, move is valid
        if (move->pos.row == toPos.row && move->pos.col == toPos.col)
        {
            //change new position
            gameState->fields[toPos.row][toPos.col].color = gameState->whoseTurn;
            gameState->fields[toPos.row][toPos.col].type = gameState->fields[fromPos.row][fromPos.col].type;

            //old position is now empty
            gameState->fields[fromPos.row][fromPos.col].type = EMPTY;

            //if it was capture enemy piece is deleted
            if (move->type == CAPTURE)
            {
                gameState->fields[move->capturedEnemyPos.row][move->capturedEnemyPos.col].type = EMPTY;
            }

            return true;
        }
    }

    return false;
}

//appends all field positions that are diagonal to fromPos and within maxDistance to positions array
int appendDiagonalPositions(struct FieldPos* positions, struct FieldPos fromPos, int maxDistance)
{
    int positionsSize = 0;

    for (int i = 0 ; i < GAME_FIELD_LENGTH ; i++)
    {
        for (int j = 0 ; j < GAME_FIELD_LENGTH ; j++)
        {
            if (
                j != fromPos.col
                && abs(j - fromPos.col) == abs(i - fromPos.row)
                && abs(j - fromPos.col) <= maxDistance
            ) {
                positions[positionsSize++] = (struct FieldPos){i, j};
            }
        }
    }

    return positionsSize;
}

struct MovesList getAvailableCaptureMovesForPawn(struct GameState* gameState, struct FieldPos fromPos)
{
    Color color = gameState->fields[fromPos.row][fromPos.col].color;
    Color enemyColor = (Color)!color;
    struct MovesList moves;
    moves.size = 0;

    struct FieldPos captureMovesPos[] = {
        {fromPos.row + 2, fromPos.col + 2},
        {fromPos.row + 2, fromPos.col - 2},
        {fromPos.row - 2, fromPos.col - 2},
        {fromPos.row - 2, fromPos.col + 2}
    };
    for (int i = 0 ; (unsigned)i < sizeof(captureMovesPos) / sizeof(struct FieldPos) ; i++)
    {
        struct FieldPos toPos = captureMovesPos[i];
        int rowVecDir = (toPos.row - fromPos.row) / 2;
        int colVecDir = (toPos.col - fromPos.col) / 2;
        if (
            fieldPosValidate(toPos)
            && gameState->fields[toPos.row][toPos.col].type == EMPTY
            && gameState->fields[fromPos.row + rowVecDir][fromPos.col + colVecDir].type != EMPTY
            && gameState->fields[fromPos.row + rowVecDir][fromPos.col + colVecDir].color == enemyColor
        ) {
            struct GameMove move;
            move.type = CAPTURE;
            move.pos = toPos;
            move.capturedEnemyPos = (struct FieldPos){fromPos.row + rowVecDir, fromPos.col + colVecDir};
            moves.moves[moves.size++] = move;
        }
    }

    return moves;
}

struct MovesList getAvailableCaptureMovesForKing(struct GameState* gameState, struct FieldPos fromPos)
{
    Color color = gameState->fields[fromPos.row][fromPos.col].color;
    Color enemyColor = (Color)!color;
    struct MovesList moves;
    moves.size = 0;

    struct FieldPos diagonalToPosList[GAME_FIELD_LENGTH * 2];
    int diagonalToPosListSize = appendDiagonalPositions(diagonalToPosList, fromPos, GAME_FIELD_LENGTH);

    for (int i = 0 ; i < diagonalToPosListSize ; i++)
    {
        struct FieldPos toPos = diagonalToPosList[i];
        int diagonalDistance = abs(toPos.row - fromPos.row);
        int rowVecDir = (toPos.row - fromPos.row) / diagonalDistance;
        int colVecDir = (toPos.col - fromPos.col) / diagonalDistance;

        if (gameState->fields[toPos.row][toPos.col].type == EMPTY)
        {
            int enemiesCountBetween = 0;
            struct FieldPos captureEnemyPos;
            for (int offset = 1 ; offset < diagonalDistance ; offset++)
            {
                struct FieldPos pos;
                pos.row = fromPos.row + (offset * rowVecDir);
                pos.col = fromPos.col + (offset * colVecDir);
                if (gameState->fields[pos.row][pos.col].type != EMPTY)
                {
                    if (gameState->fields[pos.row][pos.col].color == enemyColor)
                    {
                        enemiesCountBetween++;
                        captureEnemyPos = pos;
                    }
                    //else client's piece is between capture position and current position,
                    //so capture can't be done
                    else
                    {
                        enemiesCountBetween = 999;
                        break;
                    }
                }
            }

            if (enemiesCountBetween == 1)
            {
                struct GameMove move;
                move.type = CAPTURE;
                move.pos = toPos;
                move.capturedEnemyPos = captureEnemyPos;
                moves.moves[moves.size++] = move;
            }
        }
    }

    return moves;
}

void appendAvailableRegularMoves(struct MovesList* moves, struct GameState* gameState, struct FieldPos fromPos, int maxDistance, int verticalDir)
{
    if (!gameAPIHasToCapture(gameState))
    {
        struct FieldPos diagonalToPosList[GAME_FIELD_LENGTH * 2];
        int diagonalToPosListSize = appendDiagonalPositions(diagonalToPosList, fromPos, maxDistance);

        for (int i = 0 ; i < diagonalToPosListSize ; i++)
        {
            struct FieldPos toPos = diagonalToPosList[i];
            int diagonalDistance = abs(toPos.row - fromPos.row);
            int rowVecDir = (toPos.row - fromPos.row) / diagonalDistance;
            int colVecDir = (toPos.col - fromPos.col) / diagonalDistance;
            
            if (gameState->fields[toPos.row][toPos.col].type == EMPTY && verticalDir * rowVecDir >= 0)
            {
                bool pieceBetween = false;
                for (int offset = 1 ; offset < diagonalDistance && !pieceBetween ; offset++)
                {
                    struct FieldPos pos;
                    pos.row = fromPos.row + (offset * rowVecDir);
                    pos.col = fromPos.col + (offset * colVecDir);
                    pieceBetween = (gameState->fields[pos.row][pos.col].type != EMPTY);
                }

                if (!pieceBetween)
                {
                    struct GameMove move;
                    move.type = REGULAR;
                    move.pos = toPos;
                    moves->moves[moves->size++] = move;
                }
            }
        }
    }
}

struct MovesList getAvailableMovesForPawn(struct GameState* gameState, struct FieldPos fromPos)
{
    struct MovesList moves = getAvailableCaptureMovesForPawn(gameState, fromPos);
    Color color = gameState->fields[fromPos.row][fromPos.col].color;
    appendAvailableRegularMoves(&moves, gameState, fromPos, 1, pow(-1, color != WHITE));

    return moves;
}

struct MovesList getAvailableMovesForKing(struct GameState* gameState, struct FieldPos fromPos)
{
    struct MovesList moves = getAvailableCaptureMovesForKing(gameState, fromPos);
    appendAvailableRegularMoves(&moves, gameState, fromPos, GAME_FIELD_LENGTH, 0);

    return moves;
}

struct MovesList gameAPIgetAvailableMoves(struct GameState* gameState, struct FieldPos fromPos)
{
    FieldType fieldType = gameState->fields[fromPos.row][fromPos.col].type;
    struct MovesList moves;
    moves.size = 0;
    
    switch (fieldType)
    {
        case PAWN:
            return getAvailableMovesForPawn(gameState, fromPos);
        case KING:
            return getAvailableMovesForKing(gameState, fromPos);
        case EMPTY:
        default:
            return moves;
    }
}

bool gameAPIHasToCapture(struct GameState* gameState)
{
    for (int i = 0 ; i < GAME_FIELD_LENGTH ; i++)
    {
        for (int j = 0 ; j < GAME_FIELD_LENGTH ; j++)
        {
            if (gameState->fields[i][j].type == EMPTY || gameState->fields[i][j].color != gameState->whoseTurn)
            {
                continue;
            }

            struct MovesList availableMoves = (gameState->fields[i][j].type == PAWN) ?
                getAvailableCaptureMovesForPawn(gameState, (struct FieldPos){i, j})
                :
                getAvailableCaptureMovesForKing(gameState, (struct FieldPos){i, j});
            for (int k = 0 ; k < availableMoves.size ; k++)
            {
                if (availableMoves.moves[k].type == CAPTURE)
                {
                    return true;
                }
            }
        }
    }

    return false;
}

bool gameAPIHasToCaptureFromPos(struct GameState* gameState, struct FieldPos from)
{
    struct MovesList moves = gameAPIgetAvailableMoves(gameState, from);

    return (moves.size && moves.moves[0].type == CAPTURE);
}

void gameAPINextTurn(struct GameState* gameState)
{
    gameState->whoseTurn = (Color)!gameState->whoseTurn;
}

bool gameAPIHasGameEnded(struct GameState* gameState, Color* winner)
{
    int availableMovesCount[(int)fmax(WHITE, BLACK) + 1];
    availableMovesCount[WHITE] = availableMovesCount[BLACK] = 0;
    struct GameState tempGameState = *gameState;

    for (int i = 0 ; i < GAME_FIELD_LENGTH ; i++)
    {
        for (int j = 0 ; j < GAME_FIELD_LENGTH ; j++)
        {
            if (gameState->fields[i][j].type != EMPTY)
            {
                tempGameState.whoseTurn = tempGameState.fields[i][j].color;
                struct MovesList moves = gameAPIgetAvailableMoves(&tempGameState, (struct FieldPos){i, j});
                availableMovesCount[tempGameState.whoseTurn] += moves.size;

                //if at leat one of player have available moves game isn't ended
                if (availableMovesCount[WHITE] && availableMovesCount[BLACK])
                {
                    return false;
                }
            }
        }
    }

    //one of player have no more moves available, player with 0 moves loses
    *winner = (availableMovesCount[WHITE]) ? WHITE : BLACK;

    return true;
}

struct GameMove gameAPIGetMoveFromTo(
    struct GameState* gameState,
    struct FieldPos from,
    struct FieldPos to,
    bool* found
) {
    struct MovesList moves = gameAPIgetAvailableMoves(gameState, from);
    for (int i = 0 ; i < moves.size ; i++)
    {
        if (moves.moves[i].pos.row == to.row && moves.moves[i].pos.col == to.col)
        {
            *found = true;
            return moves.moves[i];
        }
    }
    *found = true;

    return (struct GameMove){};
}