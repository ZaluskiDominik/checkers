#ifndef GAME_API_H
#define GAME_API_H

#include "game-state.h"
#include "moves-list.h"

/*
Position - (row, column)
Black pieces are on the top of the field - starting from (0, 0) position
White pieces are on the bottom of the field - starting from (7, 0) position
*/

//modify gameState by making move from "fromPos" to "toPos", returns true if move could be made
bool gameAPIMove(struct GameState* gameState, struct FieldPos fromPos, struct FieldPos toPos);

//returns list of available moves from given position
struct MovesList gameAPIgetAvailableMoves(struct GameState* gameState, struct FieldPos fromPos);

//returns true if there are any capture moves for players in player's turn
bool gameAPIHasToCapture(struct GameState* gameState);

bool gameAPIHasToCaptureFromPos(struct GameState* gameState, struct FieldPos from);

void gameAPINextTurn(struct GameState* gameState);

bool gameAPIHasGameEnded(struct GameState* gameState, Color* winner);

//returns info about single move from "fromPos" to "toPos", "found" is set to true if that move was found
struct GameMove gameAPIGetMoveFromTo(
    struct GameState* gameState,
    struct FieldPos from,
    struct FieldPos to,
    bool* found
);

#endif