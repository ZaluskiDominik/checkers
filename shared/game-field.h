#ifndef GAME_PIECE_H
#define GAME_PIECE_H

#include "color.h"

typedef enum FieldType
{
    PAWN = 0,
    KING,
    EMPTY 
} FieldType;

struct GameField
{
    Color color;
    FieldType type;
};

#endif