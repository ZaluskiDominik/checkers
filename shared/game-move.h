#ifndef GAME_MOVE_H
#define GAME_MOVE_H

#include "field-pos.h"

typedef enum MoveType
{
    REGULAR = 0,
    CAPTURE
} MoveType;

struct GameMove
{
    struct FieldPos pos;
    MoveType type;
    struct FieldPos capturedEnemyPos;
};

#endif