#include "game-state.h"
#include <string.h>
#include <arpa/inet.h>

void gameStateInit(struct GameState* gameState)
{
    struct GameField field;
    gameState->whoseTurn = WHITE;

    for (int i = 0 ; i < GAME_FIELD_LENGTH ; i++)
    {
        for (int j = 0 ; j < GAME_FIELD_LENGTH ; j++)
        {
            if (i < 3 && j % 2 == (i+1) % 2)
            {
                field.color = WHITE;
                field.type = PAWN;
            }
            else if (i >= GAME_FIELD_LENGTH - 3 && j % 2 == (i+1) % 2)
            {
                field.color = BLACK;
                field.type = PAWN;
            }
            else
            {
                field.type = EMPTY;
            }

            gameState->fields[i][j] = field;
        }
    }
}

void gameStateSerialize(const struct GameState* gameState, unsigned char* serializedGameState)
{
    int offset = 0;
    for (int i = 0 ; i < GAME_FIELD_LENGTH ; i++)
    {
        for (int j = 0 ; j < GAME_FIELD_LENGTH ; j++)
        {
            uint32_t fieldTypeCode = htonl((uint32_t)gameState->fields[i][j].type);
            memcpy(serializedGameState + offset, &fieldTypeCode, sizeof(uint32_t));
            offset += sizeof(uint32_t);
            uint32_t colorCode = htonl((uint32_t)gameState->fields[i][j].color);
            memcpy(serializedGameState + offset, &colorCode, sizeof(uint32_t));
            offset += sizeof(uint32_t);
        }
    }

    uint32_t whoseTurnCode = htonl((uint32_t)gameState->whoseTurn);
    memcpy(serializedGameState + offset, &whoseTurnCode, sizeof(uint32_t));    
}

struct GameState gameStateUnserialize(const unsigned char* serializedGameState)
{
    struct GameState gameState;
    uint32_t* ptr = (uint32_t*)serializedGameState;
    
    for (int i = 0 ; i < GAME_FIELD_LENGTH ; i++)
    {
        for (int j = 0 ; j < GAME_FIELD_LENGTH ; j++)
        {
            struct GameField field;
            field.type = (FieldType)ntohl(*ptr++);
            field.color = (Color)ntohl(*ptr++);
            gameState.fields[i][j] = field;
        }
    }
    gameState.whoseTurn = (Color)ntohl(*ptr++);

    return gameState;
}