#ifndef GAME_STATE_H
#define GAME_STATE_H

#include "game-field.h"
#include "field-pos.h"
#include <stdint.h>

struct GameState
{
    struct GameField fields[GAME_FIELD_LENGTH][GAME_FIELD_LENGTH];
    Color whoseTurn;
};

#define GAME_STATE_SERIALIZED_SIZE (sizeof(struct GameState))

void gameStateInit(struct GameState* gameState);

void gameStateSerialize(const struct GameState* gameState, unsigned char* serializedGameState);

struct GameState gameStateUnserialize(const unsigned char* serializedGameState);

#endif