#ifndef POS_LIST_H
#define POS_LIST_H

#include "game-move.h"

#define MOVES_LIST_MAX_SIZE (GAME_FIELD_LENGTH * GAME_FIELD_LENGTH)

struct MovesList
{
    struct GameMove moves[MOVES_LIST_MAX_SIZE];
    int size;
};

#endif