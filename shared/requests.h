#ifndef REQUESTS_H
#define REQUESTS_H

typedef enum Request
{
    INVALID = -1,
    JOIN_GAME = 0,
    MOVE,
    REQUESTS_COUNT
} Request;

#endif