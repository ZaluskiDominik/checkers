#ifndef RESPONSE_STATUS_H
#define RESPONSE_STATUS_H

typedef enum ResponseStatus
{
    SUCCESS = 0,
    FAILURE
} ResponseStatus;

#endif